package beans;

import com.googlecode.objectify.annotation.*;

@Entity
public class UserBean {
	
	
	@Id String mail;
	String username; 
	String name;
	String surname;
	String password;
	String regid;
	String type; // iOS ou Android
	
	public UserBean() { }
	
	public UserBean(String username, String name, String surname, String password, String regid, String mail, String type){
		this.username=username;
		this.name=name;
		this.mail=mail;
		this.surname=surname;
		this.password=password;
		this.regid=regid;
	}
	
	public UserBean(String username, String name, String surname, String password, String mail, String type){
		this(username, name, surname,password, "", mail, type);
	}
	
	public UserBean(String username, String password, String mail){
		this(username, "", "", password, "", mail, "");
	}
	
	public UserBean(String mail, String password){
		this("", "", "", password, "", mail, "");
	}
	
	public static UserBean createDummyUserBean(String regid){
		UserBean result = new UserBean("dummy","dummy");
		result.setRegid(regid);
		return result;
	}

	
	@Override
	public String toString() {
		return "UserBean [username=" + username + ", mail="
				+ mail + ", name=" + name + ", surname=" + surname + "]";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String phoneNumber) {
		this.mail = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRegid() {
		return regid;
	}

	public void setRegid(String regid) {
		this.regid = regid;
	}
}
