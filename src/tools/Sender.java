package tools;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sun.security.action.GetLongAction;
import beans.UserBean;

import com.google.appengine.api.urlfetch.HTTPHeader;
import com.google.appengine.api.urlfetch.HTTPMethod;
import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;
import com.googlecode.objectify.ObjectifyService;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class Sender {
	
	static{
		ObjectifyService.register(UserBean.class);
	}
	
	private static String authorizationKey = "key=AIzaSyAOesxgNFo_n01c-9ShczS7VYj7x0kUuqg";
	
	private static List<String> regIds;	

	/**
	 * Envoie le message sous forme d'un String, contenant un JSONObject, qui peut �tre "fabriqu�" par la m�thode "makeMessage"
	 * @param message
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public static String[] sendMessage(String message) throws IOException, JSONException{	
		//Methode post vers les serveurs de GCM (Google Cloud Messaging)
		HTTPRequest request=new HTTPRequest(new URL("https://android.googleapis.com/gcm/send"),HTTPMethod.POST);
		//La requ�te a un "payload" et deux "headers"
	    request.setPayload((message).getBytes("UTF-8"));
	    request.addHeader(new HTTPHeader("Authorization", authorizationKey));
	    request.addHeader(new HTTPHeader("Content-Type", "application/json"));
	    //la r�ponse des serveurs de google, afin de traiter une �ventuelle erreur.
	    HTTPResponse response=URLFetchServiceFactory.getURLFetchService().fetch(request);

		return getStatus(response);
	}

	/**
	 * Renvoie le statut de la requ�te Http effectu�e, dont la r�ponse est 'response'.
	 * 
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public static String[] getStatus(HTTPResponse response) throws IOException, JSONException{
		String[] result = new String[2];
		String status = "" + response.getResponseCode();
		String content = new String(response.getContent());
		/*if(response.getResponseCode() == 200){
			JSONObject result = new JSONObject(content);
			status =status+"\nmulticastid: "+result.getInt("multicast_id");
			status=status+"\nsuccess: "+ result.getInt("success");
			status=status+ "\nfailure: "+ result.getInt("failure");
			JSONArray array = result.getJSONArray("results");
			status=status+"\n"+array;
		}*/
		result[0] = status;
		result[1] = content;

		return result;
	}

	/**
	 * Fabrique le JSONObject � donner � la m�thode 'sendMessage'.
	 * @param title   Le  titre de la notification (Si trop long, il n'apparait pas en entier. La longueur max d�pend du t�l�phone.
	 * @param contenu Le texte de la notification
	 * @return
	 * @throws JSONException
	 */
	public static String makeMessage(String title,String contenu) throws JSONException{
		
		getListRegid();
		
		JSONObject json = new JSONObject();
		
		json.put("registration_ids", regIds);


		JSONObject jsonObj = new JSONObject();
		jsonObj.put("msg", contenu);
		jsonObj.put("title", title);
		json.put("data",jsonObj );

		System.out.println(json);

		return json.toString();
	}
	
	public static List<String> getListRegid(){
		regIds = new ArrayList<String>();

		/*List<UserBean> users = ofy().load().type(UserBean.class).list();
		for(UserBean user : users){
			if(user.getRegid()!=null && user.getRegid()!=""){
				regIds.add(user.getRegid());
			}
		}*/
		regIds.add("APA91bFTRJFqk0iwZKsuCMiIhpTEV6GDH5CLSdF0wBgimDcANLdHOfR0dSeiXA696XiYrPJr4qrXC5FkujA_wrL2q3H6pXC6nM92BEtYad8Rm_Hcjbtd1sLHAuUOQm7eouaZJzJqtf1iBeYbJc93mlPOVmQIZJls7BxxZqbPCy3zL1dFerr8Rsg	");
		return regIds;
	}

	public static void main(String[] args) throws IOException, JSONException {
		String notif = makeMessage("Premier essai Cartel2015","Ceci est une premi�re notification");
		String[] status = sendMessage(notif);
		System.out.println(status);
	}

}
