package servlets;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.ObjectifyService;

import beans.UserBean;
import static com.googlecode.objectify.ObjectifyService.ofy;

public class ConnectUserServlet extends HttpServlet{

	static{
		ObjectifyService.register(UserBean.class);
	}

	private static final String MAIL = "mail";
	private static final String PASSWORD = "password";

	Logger log = Logger.getLogger(ConnectUserServlet.class.getName());

	public void doPost(HttpServletRequest request, HttpServletResponse resp) throws IOException{
		String password = request.getParameter(PASSWORD);
		log.warning("password= " + password);
		String mail = request.getParameter(MAIL);
		log.warning("mail= " + mail);

		UserBean user = ofy().load().type(UserBean.class).id(mail).now();
		if(user!=null){
			if(user.getPassword().equals(password)){
				resp.setStatus(HttpServletResponse.SC_OK);
				resp.setContentType("text/plain");
				resp.addHeader("connected", "true");
				resp.addHeader("mail", mail);
			}
			else{
				resp.setStatus(HttpServletResponse.SC_OK);
				resp.setContentType("text/plain");
				resp.addHeader("connected", "wrong_pw");
				resp.addHeader("mail", mail);
			}
		}
		else{
			resp.setStatus(HttpServletResponse.SC_OK);
			resp.setContentType("text/plain");
			resp.addHeader("connected", "wrong_user");
			resp.addHeader("mail", mail);
		}

	}
}
