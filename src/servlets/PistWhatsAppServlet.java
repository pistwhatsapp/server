package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import beans.UserBean;

import com.googlecode.objectify.ObjectifyService;

import tools.Datastore;
import static com.googlecode.objectify.ObjectifyService.ofy;

@SuppressWarnings("serial")
public class PistWhatsAppServlet extends BaseServlet {
	static{
		ObjectifyService.register(UserBean.class);
	}

	private static final String PARAMETER_REG_ID = "regId";
	private static final String USERNAME = "username";
	private static final String NAME = "name";
	private static final String SURNAME = "surname";
	private static final String TYPE = "type";
	private static final String MAIL = "mail";
	private static final String PASSWORD = "password";

	Logger log = Logger.getLogger(PistWhatsAppServlet.class.getName());

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException{
		//String regId = getParameter(request, PARAMETER_REG_ID);
		//String username = getParameter(request, USERNAME);
		//String name = getParameter(request, NAME);
		//String surname = getParameter(request, SURNAME);

		//String type = getParameter(request, TYPE);

		//Datastore.register(regId);

		String password = request.getParameter(PASSWORD);
		log.warning("password= " + password);
		String mail = request.getParameter(MAIL);
		log.warning("mail= " + mail);
		if(password!=null && mail!=null){
			UserBean u = new UserBean(mail,password);
			ofy().save().entity(u).now();
		}
		String regid = request.getParameter(PARAMETER_REG_ID);
		if(regid!=null){
			UserBean u = UserBean.createDummyUserBean(regid);
			ofy().save().entity(u).now();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException{
		String password = getParameter(request, PASSWORD);
		String mail = getParameter(request, MAIL);
		if(mail!=null && password!=null && mail.length()>0 && password.length()>0 && !mail.equals(null) && !password.equals(null)){
			UserBean u = new UserBean(mail,password);
			ofy().save().entity(u).now();
			setSuccess(response);
		}

	}
}
