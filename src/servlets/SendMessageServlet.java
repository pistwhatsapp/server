package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import tools.Sender;
import tools.SmackCcsClient;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class SendMessageServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		this.getServletContext().getRequestDispatcher("/WEB-INF/formulaire.html").forward(req, resp);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		/*String title = req.getParameter("title");
		String message = req.getParameter("content");
		
		try {
			String[] status = Sender.sendMessage(Sender.makeMessage(title, message));
			resp.getWriter().println("Message envoy� !");
			displayResults(status, resp);
		} catch (JSONException | com.google.appengine.labs.repackaged.org.json.JSONException e) {
			resp.getWriter().println("erreur");
		}*/
		try {
			PrintWriter writer = resp.getWriter();
			writer.println(Sender.getListRegid().size());
			writer.println(Sender.getListRegid().get(0));
			SmackCcsClient.testMessage();
			
			//Sender.sendMessage(Sender.makeMessage("bonjour", "essai"));

		} catch (Exception e) {
			
		}
	}
	
	private static void displayResults(String[] jsonContent, HttpServletResponse resp) throws IOException, com.google.appengine.labs.repackaged.org.json.JSONException{
		String status = jsonContent[0];
	
		PrintWriter writer = resp.getWriter();
		
		writer.println(status);
		
		List<String> list = new ArrayList<String>();
		JSONObject result = new JSONObject(jsonContent[1]);
		writer.println("multicastid: " + result.getInt("multicast_id"));
		writer.println("success: " + result.getInt("success"));
		writer.println("failure: " + result.getInt("failure"));
		JSONArray array  = result.getJSONArray("results");
		for(int i = 0; i<array.length(); i++){
			JSONObject object = array.getJSONObject(i);
			if(object.has("error")){
				writer.println("erreur: " + object.getString("error"));
			}
			else{
				writer.println("message_id: " + object.getString("message_id"));
			}
		}		
	}

}
