package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.googlecode.objectify.ObjectifyService.ofy;
import beans.UserBean;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;

public class ObjectifyTestServlet extends HttpServlet{
	
	static{
		ObjectifyService.register(UserBean.class);
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		/*UserBean u = new UserBean("mattcitron", "lemonnier", "matthieu", "ahah");
		ofy().save().entity(u).now();
		UserBean uDisplay = ofy().load().type(UserBean.class).id("mattcitron").now();*/
		
		String user = request.getParameter("surname");
		request.setAttribute("surname", user);
		String name = request.getParameter("name");
		request.setAttribute("name", name);
		this.getServletContext().getRequestDispatcher("/WEB-INF/objectify_test.jsp").forward(request, response);	
	}
}
