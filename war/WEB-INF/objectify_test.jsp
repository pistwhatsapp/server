<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Utilisateur</title>
</head>

<body>
	<h1>Test d'objectify :</h1>
	<h3>
		<%
			String username = (String) request.getAttribute("surname");
			String name = (String) request.getAttribute("name");
			out.println("Bonjour " + username + " " + name + " !");

		%>

	</h3>

	<table>
		<tr>
			<td colspan="2" style="font-weight: bold;">Available Servlets:</td>
		</tr>
		<tr>
			<td><a href="pistwhatsapp">PistWhatsApp</a></td>
		</tr>
	</table>
</body>
</html>
